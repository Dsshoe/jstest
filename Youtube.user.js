// ==UserScript==
// @name         Youtube
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.youtube.com/
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    console.log('YT TamperMonkey Started')
    window.p = document.createElement('button');
    window.ul = document.createElement('ul');
    window.header = document.querySelector('ytd-masthead');
    var test = require('test')

    window.obj = new Object;
    let p = window.p
    p.innerHTML = '&times;';
    p.style.color = 'white';
    p.style.fontFamily = 'Sans'
    p.style.fontSize = '20pt';
    p.style.width = '40px';
    p.style.height = '40px';
    p.style.marginRight = '8px';
    p.style.background = 'none';
    p.style.border = 'none';
    p.style.display = 'block';

    p.appendChild(ul);

    ul.style.background = '#080808';
    ul.style.border = '1px solid #333';
    ul.style.position = 'absolute';
    ul.style.display = 'grid';
    ul.style.width = '240px'
    ul.style.top = '48px'
    ul.style.right = '20px'
    ul.style.padding = '4px'
    ul.hidden = true;

    p.onmousedown = function(event){event.stopPropagation(); event.preventDefault(); ul.hidden = false};
    document.body.onmousedown = function(){ul.hidden = true};

    window.style = document.createElement('style')
    document.head.appendChild(style)
    style.sheet.insertRule('li:hover {color: #fff;}')

    window.getDismissedChannels = function(){
        let dismissedChannels = JSON.parse(localStorage.dismissedChannels)
        return dismissedChannels
    }

    window.addDismissedChannel = function(channelName) {
        let dismissedChannels = getDismissedChannels()
        console.log('add')
        if(!dismissedChannels.includes(channelName)){
            localStorage.dismissedChannels = JSON.stringify(dismissedChannels.concat(channelName))
        }
        if(!obj[channelName]) {obj[channelName] = 0}
        return(getDismissedChannels())
    }

    window.removeDismissedChannel = function(channelName) {
        let dismissedChannels = getDismissedChannels();
        dismissedChannels = dismissedChannels.filter(eachChannel => eachChannel != channelName);
        localStorage.dismissedChannels = JSON.stringify(dismissedChannels)
        return(getDismissedChannels())
    }

    function createButton(video){
        let x = document.createElement('div');
        let thumbnail = video.querySelector('#thumbnail')
        thumbnail.appendChild(x);
        let channelName = video.querySelector('#channel-name [title]').attributes.title.textContent
        x.classList.add('dismiss-button');
        x.textContent = 'x';
        x.style.position = 'absolute';
        x.style['font-size'] = '18pt';
        x.style['text-align'] = 'center';
        x.style.margin = '4px';
        x.style.background = '#000d';
        x.style['font-family'] = 'monospace';
        x.style.width = '28px';
        x.style.height = '28px';
        x.style['line-height'] = '24px';

        x.style.opacity = '0';

        x.onmouseenter = function(){console.log(channelName)}
        video.onmouseenter = function(event){event.target.querySelector('.dismiss-button').style.opacity = '1'}
        video.onmouseleave = function(event){event.target.querySelector('.dismiss-button').style.opacity = '0'}

        x.addEventListener('click', function(event){
            event.stopPropagation();
            event.preventDefault();
            addDismissedChannel(channelName);
            pruneVideos(loadedVideos)
            return false;
        })
    }

    function pruneSections(sectionList){
        sectionList.forEach(section => {
            section.style.display = 'none'
            let title = section.querySelector('#title').textContent;
            console.log(title, section, dismissedSections)
            if(dismissedSections.includes(title)){
                console.log('Removing Section:', title, section)
                section.style.display = 'none'
            };
        });
        return false;
    }

    function unPruneVideo(channelName) {
        removeDismissedChannel(channelName)
        loadedVideos.forEach(video => {
            let title = video.querySelector('.ytd-channel-name [title]').attributes.title.textContent;
            if(Boolean(channelName == title)){
                video.style.display = '';
                console.log('Unhidding Video:', title, video);
                return video;
            };
        });
        refreshList();
        //         window.loadedVideos = Array.prototype.slice.call(document.querySelectorAll('ytd-rich-item-renderer'));
        createList();
    }

    function pruneVideos(videoList) {
        videoList.forEach(video => {
            let dismissedVideos = getDismissedChannels();
            let title = video.querySelector('.ytd-channel-name [title]').attributes.title.textContent;
            if(Boolean(dismissedVideos.includes(title))){
                video.style.display = 'none';
                video.remove();
                obj[title] += 1
                console.log('Removing Video:', title, video);
                return video;
            };
        });
        createList();
    }

    window.createList = function(){
        while(ul.childNodes.length){ul.childNodes[0].remove()}
        getDismissedChannels().forEach(function(channelName){
            let button = document.createElement('button')
            window.ul.appendChild(button)
            button.textContent = channelName;
            button.title = channelName
            button.style['font-size'] = '12pt';
            button.style['list-style-type'] = 'none';
            button.style.padding = '4px'
            button.style.color = '#fff';
            button.style.opacity = '.75';
            button.style.background = 'transparent';
            button.style.border = 'none';
            button.style['text-align'] = 'left';
            button.onmousedown = function(){console.log(unPruneVideo(channelName))}
            button.onmouseenter = function(){button.style.opacity = '1'}
            button.onmouseleave = function(){button.style.opacity = '.75'}
            let div = document.createElement('div');
            div.textContent = `${obj[channelName]}`;
            div.style.float = 'right';
            div.style.display = 'inline';
            div.style.color = '#888';
            button.append(div)

        })
    }

    function pruneItems(itemList){

        let newVideoList = itemList.filter(item => item.localName == 'ytd-rich-item-renderer');
        let newSectionList = itemList.filter(item => item.localName == 'ytd-rich-section-renderer');

        newVideoList.forEach(video => createButton(video))

        pruneVideos(newVideoList);
        pruneSections(newSectionList);

        loadedVideos = loadedVideos.concat(newVideoList)
        loadedSections = loadedSections.concat(newSectionList)
    }

    let dismissedVideos = ['YouTube Movies'];
    let dismissedSections = ['Latest YouTube posts', 'Top picks']

    window.loadedSections = Array.prototype.slice.call(document.querySelectorAll('ytd-rich-section-renderer'));
    window.loadedVideos = Array.prototype.slice.call(document.querySelectorAll('ytd-rich-item-renderer'));

    loadedVideos.forEach(video => createButton(video))

    dismissedVideos.forEach(channel => addDismissedChannel(channel))
    getDismissedChannels().forEach(channel => obj[channel] = 0);

    pruneVideos(loadedVideos)
    pruneSections(loadedSections)

    //createList();

    function refreshList() {
        window.loadedVideos = Array.prototype.slice.call(document.querySelectorAll('ytd-rich-item-renderer'));
        window.loadedVideos.forEach(video => createButton(video))
    }

    function callback(mutations, observer) {
        mutations.forEach((mutation) => {
            let itemList = Array.prototype.slice.call(mutation.addedNodes)
            pruneItems(itemList);
            refreshList();
            console.log(itemList);
        });
    }

    var gridRendererContent = document.querySelector('ytd-rich-grid-renderer #contents')
    var observerOptions = {childList: true}
    var observer = new MutationObserver(callback);
    observer.observe(gridRendererContent, observerOptions);

    let header_end = document.querySelector('ytd-masthead #end #buttons');
    header_end.insertBefore(p, header_end.childNodes[0]);

})();